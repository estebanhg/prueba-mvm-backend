const express=require('express')
const app=express()
const cors = require('cors')
const morgan=require('morgan') //nos permite saber las peticiones en consola 



//
app.use(cors({origin:'*'}))
app.use(morgan('dev'))
app.use(express.json());


//Rutas 
app.use('/', require('./routes/Usuarios.routes'));

module.exports=app; 