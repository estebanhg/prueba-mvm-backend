const UsuariosCntrl = {}
const {getConnection} = require('../database');
const {v4} = require('uuid');


UsuariosCntrl.listar =async(req,res)=>{
    const usuarios = getConnection().get('usuarios').value();
    res.json(usuarios); 

}; 

UsuariosCntrl.listarID = async(req,res)=>{
    
    //const id = req.params.id 
    const result = await getConnection().get('usuarios').find({id: req.params.id}).value()
    res.json(result)
}


UsuariosCntrl.registrar = async(req,res)=>{
   
    const newUsuario = {
        id: v4(),
        nombre: req.body.nombre,
        apellido: req.body.apellido,
        correo: req.body.correo,
        telefono: req.body.telefono
    };

    //console.log(newUsuario);
    getConnection().get('usuarios').push(newUsuario).write();
    res.json({
        mensaje: 'Usuario creado correctamente'
    }); 
}; 


UsuariosCntrl.actualizar = async(req,res)=>{

    const result = await getConnection().get('usuarios').find({id: req.params.id})
        .assign(req.body)
        .write();

        res.json({
            mensaje: 'Usuario actualizado correctamente'
        }); 
}

UsuariosCntrl.eliminar = async(req,res)=> {

    const result = await getConnection().get('usuarios').remove({id: req.params.id}).write();

    res.json({
        mensaje: 'Usuario eliminado correctamente'
    }); 

}
module.exports=UsuariosCntrl