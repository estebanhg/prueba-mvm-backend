const app=require('./app')
const {CreateConnection} = require('./database')


//Inicio base de datos
CreateConnection();

//Declaramos el puerto 4000 diferetente a el de react 
app.set('Port',4000)

//Inicializar el servidor 

app.listen(app.get('Port'),()=>{
    console.log('Servidor escuchadno por el puerto',app.get('Port' ))
})