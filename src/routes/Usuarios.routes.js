const {Router}=require('express')
const router=Router()
const UsuariosCtrl = require('../controllers/Usuarios.controller')



router.get('/usuarios/listar',UsuariosCtrl.listar);
router.get('/usuarios/listarunico/:id',UsuariosCtrl.listarID);
router.post('/usuarios/crear',UsuariosCtrl.registrar);
router.put('/usuarios/actualizar/:id',UsuariosCtrl.actualizar);
router.delete('/usuarios/eliminar/:id',UsuariosCtrl.eliminar);

module.exports = router; 